# P5jsFlisolQuito2022

Presentación en markdown y recursos de taller para el FlISoL Quito 2022 en la UCE

# Ejercicios

## Conceptos básicos P5js 

Ejercicio 1: https://p5js.org/es/get-started/

Ejercicio 2: https://editor.p5js.org/ 

Ejercicio 3: 
https://musingswithcode.studio/generative-design-workshop/explainers/ 

Ejercicio 4: https://editor.p5js.org/p5/sketches/Hello_P5:_shapes

Ejercicio 5: 
https://editor.p5js.org/p5/sketches/Hello_P5:_interactivity

Ejercicio 6:
https://editor.p5js.org/p5/sketches/Drawing:_Pattern 

Ejercicio 7:
https://editor.p5js.org/p5/sketches/Hello_P5:_animate

Ejercicio 8:
https://editor.p5js.org/p5/sketches/Hello_P5:_drawing


## Introducción al arte generativo
https://editor.p5js.org/p5/sketches/Drawing:_Pattern

https://musingswithcode.studio/generative-design-workshop/
