---
marp: true
author: ivntres
size: 16:9
theme: 
---

# P5 el arte haciendo código
Iván Terceros 

![w:150](https://openlab.ec/sites/default/files/logos/logo1.svg)

- Esta presentación fue creada con: ![w:150 Logo marp](https://raw.githubusercontent.com/marp-team/marp/main/marp.png)

- Usa recursos de: ![w:100](https://p5js.org/assets/img/p5js.svg)

- Está bajo licencia: ![w:150](https://upload.wikimedia.org/wikipedia/commons/2/2f/CC_BY-SA_3.0.png)

- Y está disponible en: ![w:60](https://about.gitlab.com/images/press/press-kit-icon.svg)

---
# ¿Qué es la programación creativa?
![bg right](https://upload.wikimedia.org/wikipedia/commons/8/8d/Creative_coding_breakout_modification.png)
![bg](https://blogs.iadb.org/conocimiento-abierto/wp-content/uploads/sites/10/2019/06/banner-programacion-creativa-p5js.png)

---
# El arte y el código

---
# Processing
Un proyecto iniciado en 2001 por: Ben Fry y Casey Reas de Aesthetics and Computation Group del MIT Media Lab dirigido por John Maeda, basado en Java.

https://processing.org/
![w:100](https://upload.wikimedia.org/wikipedia/commons/c/cb/Processing_2021_logo.svg)

![bg right:60%](https://upload.wikimedia.org/wikipedia/commons/0/08/Processing_4.0b1_Screenshot.png)

---

![bg](https://upload.wikimedia.org/wikipedia/commons/a/ab/InfiniteMandala.jpg)
![bg](https://img.youtube.com/vi/gffiPYd6K6s/0.jpg)

---
![bg left](https://arteymedios.org/media/k2/items/cache/92c3523de61d06eebdc515f2babb64b9_XL.jpg)
# ¿Qué es P5js?
Es una interpretación de Processing para ambientes web, creada por Lauren McCarthy  en 2014. Es una biblioteca de JavaScript, enfocada a artistas, diseñadores, educadores y cualquier persona interesada en la programación creativa.

----
![bg right:60%](https://upload.wikimedia.org/wikipedia/commons/3/37/Leonardo_da_Vinci_helicopter.jpg)
# El sketch (boceto)
La lógica de programación se basa en el boceto, haciendo clara alución a un ambiente de prueba y creación.

---

# Manos a la obra
![bg](https://hackernoon.com/hn-images/1*26zGxXNsEUN5mxRN3LLbGA.png)

## bit.ly/p5flisol2022